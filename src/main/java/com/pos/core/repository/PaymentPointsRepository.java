package com.pos.core.repository;

import com.pos.core.domain.PaymentPoints;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PaymentPoints entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentPointsRepository extends JpaRepository<PaymentPoints, Long> {

}

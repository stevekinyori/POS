package com.pos.core.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.pos.core.domain.PaymentPoints;
import com.pos.core.service.PaymentPointsService;
import com.pos.core.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PaymentPoints.
 */
@RestController
@RequestMapping("/api")
public class PaymentPointsResource {

    private final Logger log = LoggerFactory.getLogger(PaymentPointsResource.class);

    private static final String ENTITY_NAME = "paymentPoints";

    private final PaymentPointsService paymentPointsService;

    public PaymentPointsResource(PaymentPointsService paymentPointsService) {
        this.paymentPointsService = paymentPointsService;
    }

    /**
     * POST  /payment-points : Create a new paymentPoints.
     *
     * @param paymentPoints the paymentPoints to create
     * @return the ResponseEntity with status 201 (Created) and with body the new paymentPoints, or with status 400 (Bad Request) if the paymentPoints has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment-points")
    @Timed
    public ResponseEntity<PaymentPoints> createPaymentPoints(@Valid @RequestBody PaymentPoints paymentPoints) throws URISyntaxException {
        log.debug("REST request to save PaymentPoints : {}", paymentPoints);
        if (paymentPoints.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new paymentPoints cannot already have an ID")).body(null);
        }
        PaymentPoints result = paymentPointsService.save(paymentPoints);
        return ResponseEntity.created(new URI("/api/payment-points/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /payment-points : Updates an existing paymentPoints.
     *
     * @param paymentPoints the paymentPoints to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated paymentPoints,
     * or with status 400 (Bad Request) if the paymentPoints is not valid,
     * or with status 500 (Internal Server Error) if the paymentPoints couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/payment-points")
    @Timed
    public ResponseEntity<PaymentPoints> updatePaymentPoints(@Valid @RequestBody PaymentPoints paymentPoints) throws URISyntaxException {
        log.debug("REST request to update PaymentPoints : {}", paymentPoints);
        if (paymentPoints.getId() == null) {
            return createPaymentPoints(paymentPoints);
        }
        PaymentPoints result = paymentPointsService.save(paymentPoints);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentPoints.getId().toString()))
            .body(result);
    }

    /**
     * GET  /payment-points : get all the paymentPoints.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of paymentPoints in body
     */
    @GetMapping("/payment-points")
    @Timed
    public List<PaymentPoints> getAllPaymentPoints() {
        log.debug("REST request to get all PaymentPoints");
        return paymentPointsService.findAll();
        }

    /**
     * GET  /payment-points/:id : get the "id" paymentPoints.
     *
     * @param id the id of the paymentPoints to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the paymentPoints, or with status 404 (Not Found)
     */
    @GetMapping("/payment-points/{id}")
    @Timed
    public ResponseEntity<PaymentPoints> getPaymentPoints(@PathVariable Long id) {
        log.debug("REST request to get PaymentPoints : {}", id);
        PaymentPoints paymentPoints = paymentPointsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentPoints));
    }

    /**
     * DELETE  /payment-points/:id : delete the "id" paymentPoints.
     *
     * @param id the id of the paymentPoints to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/payment-points/{id}")
    @Timed
    public ResponseEntity<Void> deletePaymentPoints(@PathVariable Long id) {
        log.debug("REST request to delete PaymentPoints : {}", id);
        paymentPointsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}

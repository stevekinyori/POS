package com.pos.core.domain.enumeration;

/**
 * The GENDER enumeration.
 */
public enum GENDER {
    MALE, FEMALE
}

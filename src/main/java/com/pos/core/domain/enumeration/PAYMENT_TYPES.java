package com.pos.core.domain.enumeration;

/**
 * The PAYMENT_TYPES enumeration.
 */
public enum PAYMENT_TYPES {
    BANK, MPESA, CASH, AIRTEL
}

package com.pos.core.domain.enumeration;

/**
 * The Banks enumeration.
 */
public enum Banks {
    KCB, EQUITY, COOPERATIVE, SAFARICOM, OTHER
}

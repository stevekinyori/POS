package com.pos.core.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import com.pos.core.domain.enumeration.PAYMENT_TYPES;

import com.pos.core.domain.enumeration.Banks;

/**
 * A PaymentPoints.
 */
@Entity
@Table(name = "payment_points")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PaymentPoints implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "preferred_payment_option", nullable = false)
    private PAYMENT_TYPES preferredPaymentOption;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "bank_name", nullable = false)
    private Banks bankName;

    @NotNull
    @Column(name = "account_no", nullable = false)
    private String accountNo;

    @NotNull
    @Column(name = "account_name", nullable = false)
    private String accountName;

    @OneToOne
    @JoinColumn(unique = true)
    private Employees prefPayment;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PAYMENT_TYPES getPreferredPaymentOption() {
        return preferredPaymentOption;
    }

    public PaymentPoints preferredPaymentOption(PAYMENT_TYPES preferredPaymentOption) {
        this.preferredPaymentOption = preferredPaymentOption;
        return this;
    }

    public void setPreferredPaymentOption(PAYMENT_TYPES preferredPaymentOption) {
        this.preferredPaymentOption = preferredPaymentOption;
    }

    public Banks getBankName() {
        return bankName;
    }

    public PaymentPoints bankName(Banks bankName) {
        this.bankName = bankName;
        return this;
    }

    public void setBankName(Banks bankName) {
        this.bankName = bankName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public PaymentPoints accountNo(String accountNo) {
        this.accountNo = accountNo;
        return this;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountName() {
        return accountName;
    }

    public PaymentPoints accountName(String accountName) {
        this.accountName = accountName;
        return this;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Employees getPrefPayment() {
        return prefPayment;
    }

    public PaymentPoints prefPayment(Employees employees) {
        this.prefPayment = employees;
        return this;
    }

    public void setPrefPayment(Employees employees) {
        this.prefPayment = employees;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PaymentPoints paymentPoints = (PaymentPoints) o;
        if (paymentPoints.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), paymentPoints.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PaymentPoints{" +
            "id=" + getId() +
            ", preferredPaymentOption='" + getPreferredPaymentOption() + "'" +
            ", bankName='" + getBankName() + "'" +
            ", accountNo='" + getAccountNo() + "'" +
            ", accountName='" + getAccountName() + "'" +
            "}";
    }
}

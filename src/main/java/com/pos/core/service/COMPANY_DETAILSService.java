package com.pos.core.service;

import com.pos.core.domain.COMPANY_DETAILS;
import java.util.List;

/**
 * Service Interface for managing COMPANY_DETAILS.
 */
public interface COMPANY_DETAILSService {

    /**
     * Save a cOMPANY_DETAILS.
     *
     * @param cOMPANY_DETAILS the entity to save
     * @return the persisted entity
     */
    COMPANY_DETAILS save(COMPANY_DETAILS cOMPANY_DETAILS);

    /**
     *  Get all the cOMPANY_DETAILS.
     *
     *  @return the list of entities
     */
    List<COMPANY_DETAILS> findAll();

    /**
     *  Get the "id" cOMPANY_DETAILS.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    COMPANY_DETAILS findOne(Long id);

    /**
     *  Delete the "id" cOMPANY_DETAILS.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}

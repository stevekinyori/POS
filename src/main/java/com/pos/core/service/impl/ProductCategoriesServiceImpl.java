package com.pos.core.service.impl;

import com.pos.core.service.ProductCategoriesService;
import com.pos.core.domain.ProductCategories;
import com.pos.core.repository.ProductCategoriesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ProductCategories.
 */
@Service
@Transactional
public class ProductCategoriesServiceImpl implements ProductCategoriesService{

    private final Logger log = LoggerFactory.getLogger(ProductCategoriesServiceImpl.class);

    private final ProductCategoriesRepository productCategoriesRepository;

    public ProductCategoriesServiceImpl(ProductCategoriesRepository productCategoriesRepository) {
        this.productCategoriesRepository = productCategoriesRepository;
    }

    /**
     * Save a productCategories.
     *
     * @param productCategories the entity to save
     * @return the persisted entity
     */
    @Override
    public ProductCategories save(ProductCategories productCategories) {
        log.debug("Request to save ProductCategories : {}", productCategories);
        return productCategoriesRepository.save(productCategories);
    }

    /**
     *  Get all the productCategories.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProductCategories> findAll(Pageable pageable) {
        log.debug("Request to get all ProductCategories");
        return productCategoriesRepository.findAll(pageable);
    }

    /**
     *  Get one productCategories by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ProductCategories findOne(Long id) {
        log.debug("Request to get ProductCategories : {}", id);
        return productCategoriesRepository.findOne(id);
    }

    /**
     *  Delete the  productCategories by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProductCategories : {}", id);
        productCategoriesRepository.delete(id);
    }
}

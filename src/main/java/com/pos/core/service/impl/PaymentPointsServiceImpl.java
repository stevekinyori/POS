package com.pos.core.service.impl;

import com.pos.core.service.PaymentPointsService;
import com.pos.core.domain.PaymentPoints;
import com.pos.core.repository.PaymentPointsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing PaymentPoints.
 */
@Service
@Transactional
public class PaymentPointsServiceImpl implements PaymentPointsService{

    private final Logger log = LoggerFactory.getLogger(PaymentPointsServiceImpl.class);

    private final PaymentPointsRepository paymentPointsRepository;

    public PaymentPointsServiceImpl(PaymentPointsRepository paymentPointsRepository) {
        this.paymentPointsRepository = paymentPointsRepository;
    }

    /**
     * Save a paymentPoints.
     *
     * @param paymentPoints the entity to save
     * @return the persisted entity
     */
    @Override
    public PaymentPoints save(PaymentPoints paymentPoints) {
        log.debug("Request to save PaymentPoints : {}", paymentPoints);
        return paymentPointsRepository.save(paymentPoints);
    }

    /**
     *  Get all the paymentPoints.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PaymentPoints> findAll() {
        log.debug("Request to get all PaymentPoints");
        return paymentPointsRepository.findAll();
    }

    /**
     *  Get one paymentPoints by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PaymentPoints findOne(Long id) {
        log.debug("Request to get PaymentPoints : {}", id);
        return paymentPointsRepository.findOne(id);
    }

    /**
     *  Delete the  paymentPoints by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PaymentPoints : {}", id);
        paymentPointsRepository.delete(id);
    }
}

package com.pos.core.service.impl;

import com.pos.core.service.BrandsService;
import com.pos.core.domain.Brands;
import com.pos.core.repository.BrandsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing Brands.
 */
@Service
@Transactional
public class BrandsServiceImpl implements BrandsService{

    private final Logger log = LoggerFactory.getLogger(BrandsServiceImpl.class);

    private final BrandsRepository brandsRepository;

    public BrandsServiceImpl(BrandsRepository brandsRepository) {
        this.brandsRepository = brandsRepository;
    }

    /**
     * Save a brands.
     *
     * @param brands the entity to save
     * @return the persisted entity
     */
    @Override
    public Brands save(Brands brands) {
        log.debug("Request to save Brands : {}", brands);
        return brandsRepository.save(brands);
    }

    /**
     *  Get all the brands.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Brands> findAll() {
        log.debug("Request to get all Brands");
        return brandsRepository.findAll();
    }


    /**
     *  get all the brands where Products is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<Brands> findAllWhereProductsIsNull() {
        log.debug("Request to get all brands where Products is null");
        return StreamSupport
            .stream(brandsRepository.findAll().spliterator(), false)
            .filter(brands -> brands.getProducts() == null)
            .collect(Collectors.toList());
    }

    /**
     *  Get one brands by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Brands findOne(Long id) {
        log.debug("Request to get Brands : {}", id);
        return brandsRepository.findOne(id);
    }

    /**
     *  Delete the  brands by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Brands : {}", id);
        brandsRepository.delete(id);
    }
}

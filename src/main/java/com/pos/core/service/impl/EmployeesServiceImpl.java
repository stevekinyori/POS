package com.pos.core.service.impl;

import com.pos.core.service.EmployeesService;
import com.pos.core.domain.Employees;
import com.pos.core.repository.EmployeesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing Employees.
 */
@Service
@Transactional
public class EmployeesServiceImpl implements EmployeesService{

    private final Logger log = LoggerFactory.getLogger(EmployeesServiceImpl.class);

    private final EmployeesRepository employeesRepository;

    public EmployeesServiceImpl(EmployeesRepository employeesRepository) {
        this.employeesRepository = employeesRepository;
    }

    /**
     * Save a employees.
     *
     * @param employees the entity to save
     * @return the persisted entity
     */
    @Override
    public Employees save(Employees employees) {
        log.debug("Request to save Employees : {}", employees);
        return employeesRepository.save(employees);
    }

    /**
     *  Get all the employees.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Employees> findAll() {
        log.debug("Request to get all Employees");
        return employeesRepository.findAll();
    }


    /**
     *  get all the employees where Empl is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<Employees> findAllWhereEmplIsNull() {
        log.debug("Request to get all employees where Empl is null");
        return StreamSupport
            .stream(employeesRepository.findAll().spliterator(), false)
            .filter(employees -> employees.getEmpl() == null)
            .collect(Collectors.toList());
    }

    /**
     *  Get one employees by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Employees findOne(Long id) {
        log.debug("Request to get Employees : {}", id);
        return employeesRepository.findOne(id);
    }

    /**
     *  Delete the  employees by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Employees : {}", id);
        employeesRepository.delete(id);
    }
}

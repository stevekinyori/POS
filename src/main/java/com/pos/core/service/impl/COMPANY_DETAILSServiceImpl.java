package com.pos.core.service.impl;

import com.pos.core.service.COMPANY_DETAILSService;
import com.pos.core.domain.COMPANY_DETAILS;
import com.pos.core.repository.COMPANY_DETAILSRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing COMPANY_DETAILS.
 */
@Service
@Transactional
public class COMPANY_DETAILSServiceImpl implements COMPANY_DETAILSService{

    private final Logger log = LoggerFactory.getLogger(COMPANY_DETAILSServiceImpl.class);

    private final COMPANY_DETAILSRepository cOMPANY_DETAILSRepository;

    public COMPANY_DETAILSServiceImpl(COMPANY_DETAILSRepository cOMPANY_DETAILSRepository) {
        this.cOMPANY_DETAILSRepository = cOMPANY_DETAILSRepository;
    }

    /**
     * Save a cOMPANY_DETAILS.
     *
     * @param cOMPANY_DETAILS the entity to save
     * @return the persisted entity
     */
    @Override
    public COMPANY_DETAILS save(COMPANY_DETAILS cOMPANY_DETAILS) {
        log.debug("Request to save COMPANY_DETAILS : {}", cOMPANY_DETAILS);
        return cOMPANY_DETAILSRepository.save(cOMPANY_DETAILS);
    }

    /**
     *  Get all the cOMPANY_DETAILS.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<COMPANY_DETAILS> findAll() {
        log.debug("Request to get all COMPANY_DETAILS");
        return cOMPANY_DETAILSRepository.findAll();
    }

    /**
     *  Get one cOMPANY_DETAILS by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public COMPANY_DETAILS findOne(Long id) {
        log.debug("Request to get COMPANY_DETAILS : {}", id);
        return cOMPANY_DETAILSRepository.findOne(id);
    }

    /**
     *  Delete the  cOMPANY_DETAILS by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete COMPANY_DETAILS : {}", id);
        cOMPANY_DETAILSRepository.delete(id);
    }
}

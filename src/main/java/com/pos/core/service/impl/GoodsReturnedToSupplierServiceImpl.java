package com.pos.core.service.impl;

import com.pos.core.service.GoodsReturnedToSupplierService;
import com.pos.core.domain.GoodsReturnedToSupplier;
import com.pos.core.repository.GoodsReturnedToSupplierRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing GoodsReturnedToSupplier.
 */
@Service
@Transactional
public class GoodsReturnedToSupplierServiceImpl implements GoodsReturnedToSupplierService{

    private final Logger log = LoggerFactory.getLogger(GoodsReturnedToSupplierServiceImpl.class);

    private final GoodsReturnedToSupplierRepository goodsReturnedToSupplierRepository;

    public GoodsReturnedToSupplierServiceImpl(GoodsReturnedToSupplierRepository goodsReturnedToSupplierRepository) {
        this.goodsReturnedToSupplierRepository = goodsReturnedToSupplierRepository;
    }

    /**
     * Save a goodsReturnedToSupplier.
     *
     * @param goodsReturnedToSupplier the entity to save
     * @return the persisted entity
     */
    @Override
    public GoodsReturnedToSupplier save(GoodsReturnedToSupplier goodsReturnedToSupplier) {
        log.debug("Request to save GoodsReturnedToSupplier : {}", goodsReturnedToSupplier);
        return goodsReturnedToSupplierRepository.save(goodsReturnedToSupplier);
    }

    /**
     *  Get all the goodsReturnedToSuppliers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<GoodsReturnedToSupplier> findAll(Pageable pageable) {
        log.debug("Request to get all GoodsReturnedToSuppliers");
        return goodsReturnedToSupplierRepository.findAll(pageable);
    }

    /**
     *  Get one goodsReturnedToSupplier by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public GoodsReturnedToSupplier findOne(Long id) {
        log.debug("Request to get GoodsReturnedToSupplier : {}", id);
        return goodsReturnedToSupplierRepository.findOne(id);
    }

    /**
     *  Delete the  goodsReturnedToSupplier by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete GoodsReturnedToSupplier : {}", id);
        goodsReturnedToSupplierRepository.delete(id);
    }
}

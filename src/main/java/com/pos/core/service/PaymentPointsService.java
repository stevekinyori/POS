package com.pos.core.service;

import com.pos.core.domain.PaymentPoints;
import java.util.List;

/**
 * Service Interface for managing PaymentPoints.
 */
public interface PaymentPointsService {

    /**
     * Save a paymentPoints.
     *
     * @param paymentPoints the entity to save
     * @return the persisted entity
     */
    PaymentPoints save(PaymentPoints paymentPoints);

    /**
     *  Get all the paymentPoints.
     *
     *  @return the list of entities
     */
    List<PaymentPoints> findAll();

    /**
     *  Get the "id" paymentPoints.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PaymentPoints findOne(Long id);

    /**
     *  Delete the "id" paymentPoints.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}

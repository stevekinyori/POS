package com.pos.core.service;

import com.pos.core.domain.Employees;
import java.util.List;

/**
 * Service Interface for managing Employees.
 */
public interface EmployeesService {

    /**
     * Save a employees.
     *
     * @param employees the entity to save
     * @return the persisted entity
     */
    Employees save(Employees employees);

    /**
     *  Get all the employees.
     *
     *  @return the list of entities
     */
    List<Employees> findAll();
    /**
     *  Get all the EmployeesDTO where Empl is null.
     *
     *  @return the list of entities
     */
    List<Employees> findAllWhereEmplIsNull();

    /**
     *  Get the "id" employees.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Employees findOne(Long id);

    /**
     *  Delete the "id" employees.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}

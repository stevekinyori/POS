(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('ProductsDetailController', ProductsDetailController);

    ProductsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Products', 'SubCategories', 'Brands', 'SupplierProducts'];

    function ProductsDetailController($scope, $rootScope, $stateParams, previousState, entity, Products, SubCategories, Brands, SupplierProducts) {
        var vm = this;

        vm.products = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('posApp:productsUpdate', function(event, result) {
            vm.products = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

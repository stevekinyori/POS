(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('ProductsDialogController', ProductsDialogController);

    ProductsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Products', 'SubCategories', 'Brands', 'SupplierProducts'];

    function ProductsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Products, SubCategories, Brands, SupplierProducts) {
        var vm = this;

        vm.products = entity;
        vm.clear = clear;
        vm.save = save;
        vm.subcategories = SubCategories.query({filter: 'subcategoryproducts-is-null'});
        $q.all([vm.products.$promise, vm.subcategories.$promise]).then(function() {
            if (!vm.products.subCategory || !vm.products.subCategory.id) {
                return $q.reject();
            }
            return SubCategories.get({id : vm.products.subCategory.id}).$promise;
        }).then(function(subCategory) {
            vm.subcategories.push(subCategory);
        });
        vm.brands = Brands.query({filter: 'products-is-null'});
        $q.all([vm.products.$promise, vm.brands.$promise]).then(function() {
            if (!vm.products.brand || !vm.products.brand.id) {
                return $q.reject();
            }
            return Brands.get({id : vm.products.brand.id}).$promise;
        }).then(function(brand) {
            vm.brands.push(brand);
        });
        vm.supplierproducts = SupplierProducts.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.products.id !== null) {
                Products.update(vm.products, onSaveSuccess, onSaveError);
            } else {
                Products.save(vm.products, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('posApp:productsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();

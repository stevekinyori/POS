(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('EmployeesDetailController', EmployeesDetailController);

    EmployeesDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Employees', 'PaymentPoints'];

    function EmployeesDetailController($scope, $rootScope, $stateParams, previousState, entity, Employees, PaymentPoints) {
        var vm = this;

        vm.employees = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('posApp:employeesUpdate', function(event, result) {
            vm.employees = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

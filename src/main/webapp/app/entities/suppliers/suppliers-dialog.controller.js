(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('SuppliersDialogController', SuppliersDialogController);

    SuppliersDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Suppliers', 'GoodsReturnedToSupplier', 'GoodsReceivedFromSupplier', 'SupplierProducts'];

    function SuppliersDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Suppliers, GoodsReturnedToSupplier, GoodsReceivedFromSupplier, SupplierProducts) {
        var vm = this;

        vm.suppliers = entity;
        vm.clear = clear;
        vm.save = save;
        vm.goodsreturnedtosuppliers = GoodsReturnedToSupplier.query();
        vm.goodsreceivedfromsuppliers = GoodsReceivedFromSupplier.query();
        vm.supplierproducts = SupplierProducts.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.suppliers.id !== null) {
                Suppliers.update(vm.suppliers, onSaveSuccess, onSaveError);
            } else {
                Suppliers.save(vm.suppliers, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('posApp:suppliersUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();

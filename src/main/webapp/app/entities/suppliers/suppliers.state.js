(function() {
    'use strict';

    angular
        .module('posApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('suppliers', {
            parent: 'entity',
            url: '/suppliers',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Suppliers'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/suppliers/suppliers.html',
                    controller: 'SuppliersController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('suppliers-detail', {
            parent: 'suppliers',
            url: '/suppliers/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Suppliers'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/suppliers/suppliers-detail.html',
                    controller: 'SuppliersDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Suppliers', function($stateParams, Suppliers) {
                    return Suppliers.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'suppliers',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('suppliers-detail.edit', {
            parent: 'suppliers-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/suppliers/suppliers-dialog.html',
                    controller: 'SuppliersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Suppliers', function(Suppliers) {
                            return Suppliers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('suppliers.new', {
            parent: 'suppliers',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/suppliers/suppliers-dialog.html',
                    controller: 'SuppliersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                code: null,
                                name: null,
                                mobileNo: null,
                                location: null,
                                email: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('suppliers', null, { reload: 'suppliers' });
                }, function() {
                    $state.go('suppliers');
                });
            }]
        })
        .state('suppliers.edit', {
            parent: 'suppliers',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/suppliers/suppliers-dialog.html',
                    controller: 'SuppliersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Suppliers', function(Suppliers) {
                            return Suppliers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('suppliers', null, { reload: 'suppliers' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('suppliers.delete', {
            parent: 'suppliers',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/suppliers/suppliers-delete-dialog.html',
                    controller: 'SuppliersDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Suppliers', function(Suppliers) {
                            return Suppliers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('suppliers', null, { reload: 'suppliers' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();

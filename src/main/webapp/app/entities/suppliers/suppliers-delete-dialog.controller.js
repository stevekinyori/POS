(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('SuppliersDeleteController',SuppliersDeleteController);

    SuppliersDeleteController.$inject = ['$uibModalInstance', 'entity', 'Suppliers'];

    function SuppliersDeleteController($uibModalInstance, entity, Suppliers) {
        var vm = this;

        vm.suppliers = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Suppliers.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

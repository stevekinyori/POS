(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('SuppliersDetailController', SuppliersDetailController);

    SuppliersDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Suppliers', 'GoodsReturnedToSupplier', 'GoodsReceivedFromSupplier', 'SupplierProducts'];

    function SuppliersDetailController($scope, $rootScope, $stateParams, previousState, entity, Suppliers, GoodsReturnedToSupplier, GoodsReceivedFromSupplier, SupplierProducts) {
        var vm = this;

        vm.suppliers = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('posApp:suppliersUpdate', function(event, result) {
            vm.suppliers = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

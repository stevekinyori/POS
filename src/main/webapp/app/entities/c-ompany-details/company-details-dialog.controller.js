(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('COMPANY_DETAILSDialogController', COMPANY_DETAILSDialogController);

    COMPANY_DETAILSDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'COMPANY_DETAILS'];

    function COMPANY_DETAILSDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, COMPANY_DETAILS) {
        var vm = this;

        vm.cOMPANY_DETAILS = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.cOMPANY_DETAILS.id !== null) {
                COMPANY_DETAILS.update(vm.cOMPANY_DETAILS, onSaveSuccess, onSaveError);
            } else {
                COMPANY_DETAILS.save(vm.cOMPANY_DETAILS, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('posApp:cOMPANY_DETAILSUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateOpened = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

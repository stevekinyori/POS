(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('COMPANY_DETAILSController', COMPANY_DETAILSController);

    COMPANY_DETAILSController.$inject = ['COMPANY_DETAILS'];

    function COMPANY_DETAILSController(COMPANY_DETAILS) {

        var vm = this;

        vm.cOMPANY_DETAILS = [];

        loadAll();

        function loadAll() {
            COMPANY_DETAILS.query(function(result) {
                vm.cOMPANY_DETAILS = result;
                vm.searchQuery = null;
            });
        }
    }
})();

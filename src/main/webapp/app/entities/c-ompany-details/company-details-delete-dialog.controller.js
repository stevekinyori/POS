(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('COMPANY_DETAILSDeleteController',COMPANY_DETAILSDeleteController);

    COMPANY_DETAILSDeleteController.$inject = ['$uibModalInstance', 'entity', 'COMPANY_DETAILS'];

    function COMPANY_DETAILSDeleteController($uibModalInstance, entity, COMPANY_DETAILS) {
        var vm = this;

        vm.cOMPANY_DETAILS = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            COMPANY_DETAILS.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

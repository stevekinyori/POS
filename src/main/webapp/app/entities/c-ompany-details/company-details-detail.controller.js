(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('COMPANY_DETAILSDetailController', COMPANY_DETAILSDetailController);

    COMPANY_DETAILSDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'COMPANY_DETAILS'];

    function COMPANY_DETAILSDetailController($scope, $rootScope, $stateParams, previousState, entity, COMPANY_DETAILS) {
        var vm = this;

        vm.cOMPANY_DETAILS = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('posApp:cOMPANY_DETAILSUpdate', function(event, result) {
            vm.cOMPANY_DETAILS = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

(function() {
    'use strict';

    angular
        .module('posApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('company-details', {
            parent: 'entity',
            url: '/company-details',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'COMPANY_DETAILS'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/c-ompany-details/c-ompany-details.html',
                    controller: 'COMPANY_DETAILSController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('company-details-detail', {
            parent: 'company-details',
            url: '/company-details/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'COMPANY_DETAILS'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/c-ompany-details/company-details-detail.html',
                    controller: 'COMPANY_DETAILSDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'COMPANY_DETAILS', function($stateParams, COMPANY_DETAILS) {
                    return COMPANY_DETAILS.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'company-details',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('company-details-detail.edit', {
            parent: 'company-details-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/c-ompany-details/company-details-dialog.html',
                    controller: 'COMPANY_DETAILSDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['COMPANY_DETAILS', function(COMPANY_DETAILS) {
                            return COMPANY_DETAILS.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('company-details.new', {
            parent: 'company-details',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/c-ompany-details/company-details-dialog.html',
                    controller: 'COMPANY_DETAILSDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                location: null,
                                dateOpened: null,
                                licenceNo: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('company-details', null, { reload: 'company-details' });
                }, function() {
                    $state.go('company-details');
                });
            }]
        })
        .state('company-details.edit', {
            parent: 'company-details',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/c-ompany-details/company-details-dialog.html',
                    controller: 'COMPANY_DETAILSDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['COMPANY_DETAILS', function(COMPANY_DETAILS) {
                            return COMPANY_DETAILS.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('company-details', null, { reload: 'company-details' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('company-details.delete', {
            parent: 'company-details',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/c-ompany-details/company-details-delete-dialog.html',
                    controller: 'COMPANY_DETAILSDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['COMPANY_DETAILS', function(COMPANY_DETAILS) {
                            return COMPANY_DETAILS.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('company-details', null, { reload: 'company-details' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();

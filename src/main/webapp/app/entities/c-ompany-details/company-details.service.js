(function() {
    'use strict';
    angular
        .module('posApp')
        .factory('COMPANY_DETAILS', COMPANY_DETAILS);

    COMPANY_DETAILS.$inject = ['$resource', 'DateUtils'];

    function COMPANY_DETAILS ($resource, DateUtils) {
        var resourceUrl =  'api/c-ompany-details/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateOpened = DateUtils.convertDateTimeFromServer(data.dateOpened);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();

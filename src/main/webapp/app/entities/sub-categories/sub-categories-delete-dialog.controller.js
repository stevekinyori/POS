(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('SubCategoriesDeleteController',SubCategoriesDeleteController);

    SubCategoriesDeleteController.$inject = ['$uibModalInstance', 'entity', 'SubCategories'];

    function SubCategoriesDeleteController($uibModalInstance, entity, SubCategories) {
        var vm = this;

        vm.subCategories = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SubCategories.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

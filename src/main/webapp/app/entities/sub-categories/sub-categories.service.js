(function() {
    'use strict';
    angular
        .module('posApp')
        .factory('SubCategories', SubCategories);

    SubCategories.$inject = ['$resource'];

    function SubCategories ($resource) {
        var resourceUrl =  'api/sub-categories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();

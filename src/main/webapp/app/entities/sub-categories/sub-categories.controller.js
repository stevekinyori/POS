(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('SubCategoriesController', SubCategoriesController);

    SubCategoriesController.$inject = ['SubCategories'];

    function SubCategoriesController(SubCategories) {

        var vm = this;

        vm.subCategories = [];

        loadAll();

        function loadAll() {
            SubCategories.query(function(result) {
                vm.subCategories = result;
                vm.searchQuery = null;
            });
        }
    }
})();

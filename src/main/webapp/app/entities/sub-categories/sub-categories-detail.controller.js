(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('SubCategoriesDetailController', SubCategoriesDetailController);

    SubCategoriesDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SubCategories', 'Products', 'ProductCategories'];

    function SubCategoriesDetailController($scope, $rootScope, $stateParams, previousState, entity, SubCategories, Products, ProductCategories) {
        var vm = this;

        vm.subCategories = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('posApp:subCategoriesUpdate', function(event, result) {
            vm.subCategories = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

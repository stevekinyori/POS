(function() {
    'use strict';

    angular
        .module('posApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('sub-categories', {
            parent: 'entity',
            url: '/sub-categories',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'SubCategories'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/sub-categories/sub-categories.html',
                    controller: 'SubCategoriesController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('sub-categories-detail', {
            parent: 'sub-categories',
            url: '/sub-categories/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'SubCategories'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/sub-categories/sub-categories-detail.html',
                    controller: 'SubCategoriesDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SubCategories', function($stateParams, SubCategories) {
                    return SubCategories.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'sub-categories',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('sub-categories-detail.edit', {
            parent: 'sub-categories-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-categories/sub-categories-dialog.html',
                    controller: 'SubCategoriesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SubCategories', function(SubCategories) {
                            return SubCategories.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('sub-categories.new', {
            parent: 'sub-categories',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-categories/sub-categories-dialog.html',
                    controller: 'SubCategoriesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                shortDesciption: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('sub-categories', null, { reload: 'sub-categories' });
                }, function() {
                    $state.go('sub-categories');
                });
            }]
        })
        .state('sub-categories.edit', {
            parent: 'sub-categories',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-categories/sub-categories-dialog.html',
                    controller: 'SubCategoriesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SubCategories', function(SubCategories) {
                            return SubCategories.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('sub-categories', null, { reload: 'sub-categories' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('sub-categories.delete', {
            parent: 'sub-categories',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-categories/sub-categories-delete-dialog.html',
                    controller: 'SubCategoriesDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SubCategories', function(SubCategories) {
                            return SubCategories.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('sub-categories', null, { reload: 'sub-categories' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();

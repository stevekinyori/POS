(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('SubCategoriesDialogController', SubCategoriesDialogController);

    SubCategoriesDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SubCategories', 'Products', 'ProductCategories'];

    function SubCategoriesDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SubCategories, Products, ProductCategories) {
        var vm = this;

        vm.subCategories = entity;
        vm.clear = clear;
        vm.save = save;
        vm.products = Products.query();
        vm.productcategories = ProductCategories.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.subCategories.id !== null) {
                SubCategories.update(vm.subCategories, onSaveSuccess, onSaveError);
            } else {
                SubCategories.save(vm.subCategories, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('posApp:subCategoriesUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();

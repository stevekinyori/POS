(function() {
    'use strict';

    angular
        .module('posApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('supplier-products', {
            parent: 'entity',
            url: '/supplier-products',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'SupplierProducts'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/supplier-products/supplier-products.html',
                    controller: 'SupplierProductsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('supplier-products-detail', {
            parent: 'supplier-products',
            url: '/supplier-products/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'SupplierProducts'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/supplier-products/supplier-products-detail.html',
                    controller: 'SupplierProductsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SupplierProducts', function($stateParams, SupplierProducts) {
                    return SupplierProducts.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'supplier-products',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('supplier-products-detail.edit', {
            parent: 'supplier-products-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/supplier-products/supplier-products-dialog.html',
                    controller: 'SupplierProductsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SupplierProducts', function(SupplierProducts) {
                            return SupplierProducts.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('supplier-products.new', {
            parent: 'supplier-products',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/supplier-products/supplier-products-dialog.html',
                    controller: 'SupplierProductsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                buyingPrice: null,
                                standardBuyingPrice: null,
                                retailPrice: null,
                                deliveryLeadTime: null,
                                firstItemDeliveryDate: null,
                                minimumReorderLevel: null,
                                maxReorderLevel: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('supplier-products', null, { reload: 'supplier-products' });
                }, function() {
                    $state.go('supplier-products');
                });
            }]
        })
        .state('supplier-products.edit', {
            parent: 'supplier-products',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/supplier-products/supplier-products-dialog.html',
                    controller: 'SupplierProductsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SupplierProducts', function(SupplierProducts) {
                            return SupplierProducts.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('supplier-products', null, { reload: 'supplier-products' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('supplier-products.delete', {
            parent: 'supplier-products',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/supplier-products/supplier-products-delete-dialog.html',
                    controller: 'SupplierProductsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SupplierProducts', function(SupplierProducts) {
                            return SupplierProducts.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('supplier-products', null, { reload: 'supplier-products' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();

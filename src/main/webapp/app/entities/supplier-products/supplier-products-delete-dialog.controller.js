(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('SupplierProductsDeleteController',SupplierProductsDeleteController);

    SupplierProductsDeleteController.$inject = ['$uibModalInstance', 'entity', 'SupplierProducts'];

    function SupplierProductsDeleteController($uibModalInstance, entity, SupplierProducts) {
        var vm = this;

        vm.supplierProducts = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SupplierProducts.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

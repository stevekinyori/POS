(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('SupplierProductsDialogController', SupplierProductsDialogController);

    SupplierProductsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'SupplierProducts', 'Products', 'Suppliers'];

    function SupplierProductsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, SupplierProducts, Products, Suppliers) {
        var vm = this;

        vm.supplierProducts = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.products = Products.query({filter: 'suppliers-is-null'});
        $q.all([vm.supplierProducts.$promise, vm.products.$promise]).then(function() {
            if (!vm.supplierProducts.product || !vm.supplierProducts.product.id) {
                return $q.reject();
            }
            return Products.get({id : vm.supplierProducts.product.id}).$promise;
        }).then(function(product) {
            vm.products.push(product);
        });
        vm.suppliers = Suppliers.query({filter: 'products-is-null'});
        $q.all([vm.supplierProducts.$promise, vm.suppliers.$promise]).then(function() {
            if (!vm.supplierProducts.supplier || !vm.supplierProducts.supplier.id) {
                return $q.reject();
            }
            return Suppliers.get({id : vm.supplierProducts.supplier.id}).$promise;
        }).then(function(supplier) {
            vm.suppliers.push(supplier);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.supplierProducts.id !== null) {
                SupplierProducts.update(vm.supplierProducts, onSaveSuccess, onSaveError);
            } else {
                SupplierProducts.save(vm.supplierProducts, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('posApp:supplierProductsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.firstItemDeliveryDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

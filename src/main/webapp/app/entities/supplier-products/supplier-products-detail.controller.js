(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('SupplierProductsDetailController', SupplierProductsDetailController);

    SupplierProductsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SupplierProducts', 'Products', 'Suppliers'];

    function SupplierProductsDetailController($scope, $rootScope, $stateParams, previousState, entity, SupplierProducts, Products, Suppliers) {
        var vm = this;

        vm.supplierProducts = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('posApp:supplierProductsUpdate', function(event, result) {
            vm.supplierProducts = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

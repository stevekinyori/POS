(function() {
    'use strict';
    angular
        .module('posApp')
        .factory('SupplierProducts', SupplierProducts);

    SupplierProducts.$inject = ['$resource', 'DateUtils'];

    function SupplierProducts ($resource, DateUtils) {
        var resourceUrl =  'api/supplier-products/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.firstItemDeliveryDate = DateUtils.convertLocalDateFromServer(data.firstItemDeliveryDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.firstItemDeliveryDate = DateUtils.convertLocalDateToServer(copy.firstItemDeliveryDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.firstItemDeliveryDate = DateUtils.convertLocalDateToServer(copy.firstItemDeliveryDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();

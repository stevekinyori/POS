(function() {
    'use strict';
    angular
        .module('posApp')
        .factory('ProductCategories', ProductCategories);

    ProductCategories.$inject = ['$resource', 'DateUtils'];

    function ProductCategories ($resource, DateUtils) {
        var resourceUrl =  'api/product-categories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateCreated = DateUtils.convertDateTimeFromServer(data.dateCreated);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();

(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('ProductCategoriesDeleteController',ProductCategoriesDeleteController);

    ProductCategoriesDeleteController.$inject = ['$uibModalInstance', 'entity', 'ProductCategories'];

    function ProductCategoriesDeleteController($uibModalInstance, entity, ProductCategories) {
        var vm = this;

        vm.productCategories = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ProductCategories.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

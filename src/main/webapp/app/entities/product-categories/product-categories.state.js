(function() {
    'use strict';

    angular
        .module('posApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('product-categories', {
            parent: 'entity',
            url: '/product-categories?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ProductCategories'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/product-categories/product-categories.html',
                    controller: 'ProductCategoriesController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
            }
        })
        .state('product-categories-detail', {
            parent: 'product-categories',
            url: '/product-categories/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ProductCategories'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/product-categories/product-categories-detail.html',
                    controller: 'ProductCategoriesDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'ProductCategories', function($stateParams, ProductCategories) {
                    return ProductCategories.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'product-categories',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('product-categories-detail.edit', {
            parent: 'product-categories-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/product-categories/product-categories-dialog.html',
                    controller: 'ProductCategoriesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ProductCategories', function(ProductCategories) {
                            return ProductCategories.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('product-categories.new', {
            parent: 'product-categories',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/product-categories/product-categories-dialog.html',
                    controller: 'ProductCategoriesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                code: null,
                                name: null,
                                shtDescription: null,
                                description: null,
                                dateCreated: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('product-categories', null, { reload: 'product-categories' });
                }, function() {
                    $state.go('product-categories');
                });
            }]
        })
        .state('product-categories.edit', {
            parent: 'product-categories',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/product-categories/product-categories-dialog.html',
                    controller: 'ProductCategoriesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ProductCategories', function(ProductCategories) {
                            return ProductCategories.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('product-categories', null, { reload: 'product-categories' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('product-categories.delete', {
            parent: 'product-categories',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/product-categories/product-categories-delete-dialog.html',
                    controller: 'ProductCategoriesDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ProductCategories', function(ProductCategories) {
                            return ProductCategories.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('product-categories', null, { reload: 'product-categories' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();

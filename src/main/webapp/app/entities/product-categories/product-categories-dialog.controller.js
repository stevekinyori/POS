(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('ProductCategoriesDialogController', ProductCategoriesDialogController);

    ProductCategoriesDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ProductCategories', 'SubCategories'];

    function ProductCategoriesDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ProductCategories, SubCategories) {
        var vm = this;

        vm.productCategories = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.subcategories = SubCategories.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.productCategories.id !== null) {
                ProductCategories.update(vm.productCategories, onSaveSuccess, onSaveError);
            } else {
                ProductCategories.save(vm.productCategories, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('posApp:productCategoriesUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateCreated = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('ProductCategoriesDetailController', ProductCategoriesDetailController);

    ProductCategoriesDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ProductCategories', 'SubCategories'];

    function ProductCategoriesDetailController($scope, $rootScope, $stateParams, previousState, entity, ProductCategories, SubCategories) {
        var vm = this;

        vm.productCategories = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('posApp:productCategoriesUpdate', function(event, result) {
            vm.productCategories = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

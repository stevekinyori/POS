(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('BrandsDeleteController',BrandsDeleteController);

    BrandsDeleteController.$inject = ['$uibModalInstance', 'entity', 'Brands'];

    function BrandsDeleteController($uibModalInstance, entity, Brands) {
        var vm = this;

        vm.brands = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Brands.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

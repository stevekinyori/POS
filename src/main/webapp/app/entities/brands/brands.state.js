(function() {
    'use strict';

    angular
        .module('posApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('brands', {
            parent: 'entity',
            url: '/brands',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Brands'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/brands/brands.html',
                    controller: 'BrandsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('brands-detail', {
            parent: 'brands',
            url: '/brands/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Brands'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/brands/brands-detail.html',
                    controller: 'BrandsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Brands', function($stateParams, Brands) {
                    return Brands.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'brands',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('brands-detail.edit', {
            parent: 'brands-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/brands/brands-dialog.html',
                    controller: 'BrandsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Brands', function(Brands) {
                            return Brands.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('brands.new', {
            parent: 'brands',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/brands/brands-dialog.html',
                    controller: 'BrandsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                shtDesc: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('brands', null, { reload: 'brands' });
                }, function() {
                    $state.go('brands');
                });
            }]
        })
        .state('brands.edit', {
            parent: 'brands',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/brands/brands-dialog.html',
                    controller: 'BrandsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Brands', function(Brands) {
                            return Brands.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('brands', null, { reload: 'brands' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('brands.delete', {
            parent: 'brands',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/brands/brands-delete-dialog.html',
                    controller: 'BrandsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Brands', function(Brands) {
                            return Brands.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('brands', null, { reload: 'brands' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();

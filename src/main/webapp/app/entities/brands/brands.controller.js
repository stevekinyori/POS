(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('BrandsController', BrandsController);

    BrandsController.$inject = ['Brands'];

    function BrandsController(Brands) {

        var vm = this;

        vm.brands = [];

        loadAll();

        function loadAll() {
            Brands.query(function(result) {
                vm.brands = result;
                vm.searchQuery = null;
            });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('BrandsDialogController', BrandsDialogController);

    BrandsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Brands', 'Products'];

    function BrandsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Brands, Products) {
        var vm = this;

        vm.brands = entity;
        vm.clear = clear;
        vm.save = save;
        vm.products = Products.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.brands.id !== null) {
                Brands.update(vm.brands, onSaveSuccess, onSaveError);
            } else {
                Brands.save(vm.brands, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('posApp:brandsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();

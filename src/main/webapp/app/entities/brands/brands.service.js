(function() {
    'use strict';
    angular
        .module('posApp')
        .factory('Brands', Brands);

    Brands.$inject = ['$resource'];

    function Brands ($resource) {
        var resourceUrl =  'api/brands/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();

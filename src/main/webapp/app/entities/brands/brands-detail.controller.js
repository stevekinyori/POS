(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('BrandsDetailController', BrandsDetailController);

    BrandsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Brands', 'Products'];

    function BrandsDetailController($scope, $rootScope, $stateParams, previousState, entity, Brands, Products) {
        var vm = this;

        vm.brands = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('posApp:brandsUpdate', function(event, result) {
            vm.brands = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

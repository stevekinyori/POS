(function() {
    'use strict';

    angular
        .module('posApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('payment-points', {
            parent: 'entity',
            url: '/payment-points',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'PaymentPoints'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/payment-points/payment-points.html',
                    controller: 'PaymentPointsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('payment-points-detail', {
            parent: 'payment-points',
            url: '/payment-points/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'PaymentPoints'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/payment-points/payment-points-detail.html',
                    controller: 'PaymentPointsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'PaymentPoints', function($stateParams, PaymentPoints) {
                    return PaymentPoints.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'payment-points',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('payment-points-detail.edit', {
            parent: 'payment-points-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/payment-points/payment-points-dialog.html',
                    controller: 'PaymentPointsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PaymentPoints', function(PaymentPoints) {
                            return PaymentPoints.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('payment-points.new', {
            parent: 'payment-points',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/payment-points/payment-points-dialog.html',
                    controller: 'PaymentPointsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                preferredPaymentOption: null,
                                bankName: null,
                                accountNo: null,
                                accountName: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('payment-points', null, { reload: 'payment-points' });
                }, function() {
                    $state.go('payment-points');
                });
            }]
        })
        .state('payment-points.edit', {
            parent: 'payment-points',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/payment-points/payment-points-dialog.html',
                    controller: 'PaymentPointsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PaymentPoints', function(PaymentPoints) {
                            return PaymentPoints.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('payment-points', null, { reload: 'payment-points' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('payment-points.delete', {
            parent: 'payment-points',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/payment-points/payment-points-delete-dialog.html',
                    controller: 'PaymentPointsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PaymentPoints', function(PaymentPoints) {
                            return PaymentPoints.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('payment-points', null, { reload: 'payment-points' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();

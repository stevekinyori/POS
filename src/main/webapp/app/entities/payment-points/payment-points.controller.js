(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('PaymentPointsController', PaymentPointsController);

    PaymentPointsController.$inject = ['PaymentPoints'];

    function PaymentPointsController(PaymentPoints) {

        var vm = this;

        vm.paymentPoints = [];

        loadAll();

        function loadAll() {
            PaymentPoints.query(function(result) {
                vm.paymentPoints = result;
                vm.searchQuery = null;
            });
        }
    }
})();

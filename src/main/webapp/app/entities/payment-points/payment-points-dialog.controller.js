(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('PaymentPointsDialogController', PaymentPointsDialogController);

    PaymentPointsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'PaymentPoints', 'Employees'];

    function PaymentPointsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, PaymentPoints, Employees) {
        var vm = this;

        vm.paymentPoints = entity;
        vm.clear = clear;
        vm.save = save;
        vm.prefpayments = Employees.query({filter: 'empl-is-null'});
        $q.all([vm.paymentPoints.$promise, vm.prefpayments.$promise]).then(function() {
            if (!vm.paymentPoints.prefPayment || !vm.paymentPoints.prefPayment.id) {
                return $q.reject();
            }
            return Employees.get({id : vm.paymentPoints.prefPayment.id}).$promise;
        }).then(function(prefPayment) {
            vm.prefpayments.push(prefPayment);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.paymentPoints.id !== null) {
                PaymentPoints.update(vm.paymentPoints, onSaveSuccess, onSaveError);
            } else {
                PaymentPoints.save(vm.paymentPoints, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('posApp:paymentPointsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();

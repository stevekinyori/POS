(function() {
    'use strict';
    angular
        .module('posApp')
        .factory('PaymentPoints', PaymentPoints);

    PaymentPoints.$inject = ['$resource'];

    function PaymentPoints ($resource) {
        var resourceUrl =  'api/payment-points/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();

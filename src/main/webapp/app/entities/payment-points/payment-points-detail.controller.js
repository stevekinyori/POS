(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('PaymentPointsDetailController', PaymentPointsDetailController);

    PaymentPointsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'PaymentPoints', 'Employees'];

    function PaymentPointsDetailController($scope, $rootScope, $stateParams, previousState, entity, PaymentPoints, Employees) {
        var vm = this;

        vm.paymentPoints = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('posApp:paymentPointsUpdate', function(event, result) {
            vm.paymentPoints = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('PaymentPointsDeleteController',PaymentPointsDeleteController);

    PaymentPointsDeleteController.$inject = ['$uibModalInstance', 'entity', 'PaymentPoints'];

    function PaymentPointsDeleteController($uibModalInstance, entity, PaymentPoints) {
        var vm = this;

        vm.paymentPoints = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            PaymentPoints.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

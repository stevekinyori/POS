(function() {
    'use strict';

    angular
        .module('posApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('goods-returned-to-supplier', {
            parent: 'entity',
            url: '/goods-returned-to-supplier',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'GoodsReturnedToSuppliers'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/goods-returned-to-supplier/goods-returned-to-suppliers.html',
                    controller: 'GoodsReturnedToSupplierController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('goods-returned-to-supplier-detail', {
            parent: 'goods-returned-to-supplier',
            url: '/goods-returned-to-supplier/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'GoodsReturnedToSupplier'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/goods-returned-to-supplier/goods-returned-to-supplier-detail.html',
                    controller: 'GoodsReturnedToSupplierDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'GoodsReturnedToSupplier', function($stateParams, GoodsReturnedToSupplier) {
                    return GoodsReturnedToSupplier.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'goods-returned-to-supplier',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('goods-returned-to-supplier-detail.edit', {
            parent: 'goods-returned-to-supplier-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/goods-returned-to-supplier/goods-returned-to-supplier-dialog.html',
                    controller: 'GoodsReturnedToSupplierDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['GoodsReturnedToSupplier', function(GoodsReturnedToSupplier) {
                            return GoodsReturnedToSupplier.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('goods-returned-to-supplier.new', {
            parent: 'goods-returned-to-supplier',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/goods-returned-to-supplier/goods-returned-to-supplier-dialog.html',
                    controller: 'GoodsReturnedToSupplierDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                batchNo: null,
                                quantity: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('goods-returned-to-supplier', null, { reload: 'goods-returned-to-supplier' });
                }, function() {
                    $state.go('goods-returned-to-supplier');
                });
            }]
        })
        .state('goods-returned-to-supplier.edit', {
            parent: 'goods-returned-to-supplier',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/goods-returned-to-supplier/goods-returned-to-supplier-dialog.html',
                    controller: 'GoodsReturnedToSupplierDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['GoodsReturnedToSupplier', function(GoodsReturnedToSupplier) {
                            return GoodsReturnedToSupplier.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('goods-returned-to-supplier', null, { reload: 'goods-returned-to-supplier' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('goods-returned-to-supplier.delete', {
            parent: 'goods-returned-to-supplier',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/goods-returned-to-supplier/goods-returned-to-supplier-delete-dialog.html',
                    controller: 'GoodsReturnedToSupplierDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['GoodsReturnedToSupplier', function(GoodsReturnedToSupplier) {
                            return GoodsReturnedToSupplier.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('goods-returned-to-supplier', null, { reload: 'goods-returned-to-supplier' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();

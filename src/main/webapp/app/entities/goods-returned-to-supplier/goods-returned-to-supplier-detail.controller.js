(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('GoodsReturnedToSupplierDetailController', GoodsReturnedToSupplierDetailController);

    GoodsReturnedToSupplierDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'GoodsReturnedToSupplier', 'Suppliers'];

    function GoodsReturnedToSupplierDetailController($scope, $rootScope, $stateParams, previousState, entity, GoodsReturnedToSupplier, Suppliers) {
        var vm = this;

        vm.goodsReturnedToSupplier = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('posApp:goodsReturnedToSupplierUpdate', function(event, result) {
            vm.goodsReturnedToSupplier = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

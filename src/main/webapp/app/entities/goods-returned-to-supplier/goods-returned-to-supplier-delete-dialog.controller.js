(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('GoodsReturnedToSupplierDeleteController',GoodsReturnedToSupplierDeleteController);

    GoodsReturnedToSupplierDeleteController.$inject = ['$uibModalInstance', 'entity', 'GoodsReturnedToSupplier'];

    function GoodsReturnedToSupplierDeleteController($uibModalInstance, entity, GoodsReturnedToSupplier) {
        var vm = this;

        vm.goodsReturnedToSupplier = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            GoodsReturnedToSupplier.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

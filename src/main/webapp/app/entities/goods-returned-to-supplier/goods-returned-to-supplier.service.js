(function() {
    'use strict';
    angular
        .module('posApp')
        .factory('GoodsReturnedToSupplier', GoodsReturnedToSupplier);

    GoodsReturnedToSupplier.$inject = ['$resource'];

    function GoodsReturnedToSupplier ($resource) {
        var resourceUrl =  'api/goods-returned-to-suppliers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();

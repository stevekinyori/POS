(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('GoodsReturnedToSupplierDialogController', GoodsReturnedToSupplierDialogController);

    GoodsReturnedToSupplierDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'GoodsReturnedToSupplier', 'Suppliers'];

    function GoodsReturnedToSupplierDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, GoodsReturnedToSupplier, Suppliers) {
        var vm = this;

        vm.goodsReturnedToSupplier = entity;
        vm.clear = clear;
        vm.save = save;
        vm.batchsuppliers = Suppliers.query({filter: 'goodsreturned-is-null'});
        $q.all([vm.goodsReturnedToSupplier.$promise, vm.batchsuppliers.$promise]).then(function() {
            if (!vm.goodsReturnedToSupplier.batchSupplier || !vm.goodsReturnedToSupplier.batchSupplier.id) {
                return $q.reject();
            }
            return Suppliers.get({id : vm.goodsReturnedToSupplier.batchSupplier.id}).$promise;
        }).then(function(batchSupplier) {
            vm.batchsuppliers.push(batchSupplier);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.goodsReturnedToSupplier.id !== null) {
                GoodsReturnedToSupplier.update(vm.goodsReturnedToSupplier, onSaveSuccess, onSaveError);
            } else {
                GoodsReturnedToSupplier.save(vm.goodsReturnedToSupplier, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('posApp:goodsReturnedToSupplierUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();

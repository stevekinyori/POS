(function() {
    'use strict';
    angular
        .module('posApp')
        .factory('GoodsReceivedFromSupplier', GoodsReceivedFromSupplier);

    GoodsReceivedFromSupplier.$inject = ['$resource'];

    function GoodsReceivedFromSupplier ($resource) {
        var resourceUrl =  'api/goods-received-from-suppliers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();

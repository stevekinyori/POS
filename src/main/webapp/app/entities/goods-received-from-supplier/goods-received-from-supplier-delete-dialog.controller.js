(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('GoodsReceivedFromSupplierDeleteController',GoodsReceivedFromSupplierDeleteController);

    GoodsReceivedFromSupplierDeleteController.$inject = ['$uibModalInstance', 'entity', 'GoodsReceivedFromSupplier'];

    function GoodsReceivedFromSupplierDeleteController($uibModalInstance, entity, GoodsReceivedFromSupplier) {
        var vm = this;

        vm.goodsReceivedFromSupplier = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            GoodsReceivedFromSupplier.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('GoodsReceivedFromSupplierDetailController', GoodsReceivedFromSupplierDetailController);

    GoodsReceivedFromSupplierDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'GoodsReceivedFromSupplier', 'Suppliers'];

    function GoodsReceivedFromSupplierDetailController($scope, $rootScope, $stateParams, previousState, entity, GoodsReceivedFromSupplier, Suppliers) {
        var vm = this;

        vm.goodsReceivedFromSupplier = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('posApp:goodsReceivedFromSupplierUpdate', function(event, result) {
            vm.goodsReceivedFromSupplier = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

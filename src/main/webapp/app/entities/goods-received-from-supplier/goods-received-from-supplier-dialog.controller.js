(function() {
    'use strict';

    angular
        .module('posApp')
        .controller('GoodsReceivedFromSupplierDialogController', GoodsReceivedFromSupplierDialogController);

    GoodsReceivedFromSupplierDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'GoodsReceivedFromSupplier', 'Suppliers'];

    function GoodsReceivedFromSupplierDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, GoodsReceivedFromSupplier, Suppliers) {
        var vm = this;

        vm.goodsReceivedFromSupplier = entity;
        vm.clear = clear;
        vm.save = save;
        vm.batchproducts = Suppliers.query({filter: 'receivedfromsupplier-is-null'});
        $q.all([vm.goodsReceivedFromSupplier.$promise, vm.batchproducts.$promise]).then(function() {
            if (!vm.goodsReceivedFromSupplier.batchProduct || !vm.goodsReceivedFromSupplier.batchProduct.id) {
                return $q.reject();
            }
            return Suppliers.get({id : vm.goodsReceivedFromSupplier.batchProduct.id}).$promise;
        }).then(function(batchProduct) {
            vm.batchproducts.push(batchProduct);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.goodsReceivedFromSupplier.id !== null) {
                GoodsReceivedFromSupplier.update(vm.goodsReceivedFromSupplier, onSaveSuccess, onSaveError);
            } else {
                GoodsReceivedFromSupplier.save(vm.goodsReceivedFromSupplier, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('posApp:goodsReceivedFromSupplierUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();

(function() {
    'use strict';

    angular
        .module('posApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('goods-received-from-supplier', {
            parent: 'entity',
            url: '/goods-received-from-supplier',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'GoodsReceivedFromSuppliers'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/goods-received-from-supplier/goods-received-from-suppliers.html',
                    controller: 'GoodsReceivedFromSupplierController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('goods-received-from-supplier-detail', {
            parent: 'goods-received-from-supplier',
            url: '/goods-received-from-supplier/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'GoodsReceivedFromSupplier'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/goods-received-from-supplier/goods-received-from-supplier-detail.html',
                    controller: 'GoodsReceivedFromSupplierDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'GoodsReceivedFromSupplier', function($stateParams, GoodsReceivedFromSupplier) {
                    return GoodsReceivedFromSupplier.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'goods-received-from-supplier',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('goods-received-from-supplier-detail.edit', {
            parent: 'goods-received-from-supplier-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/goods-received-from-supplier/goods-received-from-supplier-dialog.html',
                    controller: 'GoodsReceivedFromSupplierDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['GoodsReceivedFromSupplier', function(GoodsReceivedFromSupplier) {
                            return GoodsReceivedFromSupplier.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('goods-received-from-supplier.new', {
            parent: 'goods-received-from-supplier',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/goods-received-from-supplier/goods-received-from-supplier-dialog.html',
                    controller: 'GoodsReceivedFromSupplierDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                batchNo: null,
                                quantity: null,
                                unitPrice: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('goods-received-from-supplier', null, { reload: 'goods-received-from-supplier' });
                }, function() {
                    $state.go('goods-received-from-supplier');
                });
            }]
        })
        .state('goods-received-from-supplier.edit', {
            parent: 'goods-received-from-supplier',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/goods-received-from-supplier/goods-received-from-supplier-dialog.html',
                    controller: 'GoodsReceivedFromSupplierDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['GoodsReceivedFromSupplier', function(GoodsReceivedFromSupplier) {
                            return GoodsReceivedFromSupplier.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('goods-received-from-supplier', null, { reload: 'goods-received-from-supplier' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('goods-received-from-supplier.delete', {
            parent: 'goods-received-from-supplier',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/goods-received-from-supplier/goods-received-from-supplier-delete-dialog.html',
                    controller: 'GoodsReceivedFromSupplierDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['GoodsReceivedFromSupplier', function(GoodsReceivedFromSupplier) {
                            return GoodsReceivedFromSupplier.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('goods-received-from-supplier', null, { reload: 'goods-received-from-supplier' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();

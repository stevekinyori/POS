(function() {
    'use strict';

    angular
        .module('posApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();

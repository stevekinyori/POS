'use strict';

describe('Controller Tests', function() {

    describe('Products Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockProducts, MockSubCategories, MockBrands, MockSupplierProducts;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockProducts = jasmine.createSpy('MockProducts');
            MockSubCategories = jasmine.createSpy('MockSubCategories');
            MockBrands = jasmine.createSpy('MockBrands');
            MockSupplierProducts = jasmine.createSpy('MockSupplierProducts');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Products': MockProducts,
                'SubCategories': MockSubCategories,
                'Brands': MockBrands,
                'SupplierProducts': MockSupplierProducts
            };
            createController = function() {
                $injector.get('$controller')("ProductsDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'posApp:productsUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});

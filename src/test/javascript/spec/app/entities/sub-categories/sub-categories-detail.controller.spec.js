'use strict';

describe('Controller Tests', function() {

    describe('SubCategories Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockSubCategories, MockProducts, MockProductCategories;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockSubCategories = jasmine.createSpy('MockSubCategories');
            MockProducts = jasmine.createSpy('MockProducts');
            MockProductCategories = jasmine.createSpy('MockProductCategories');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'SubCategories': MockSubCategories,
                'Products': MockProducts,
                'ProductCategories': MockProductCategories
            };
            createController = function() {
                $injector.get('$controller')("SubCategoriesDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'posApp:subCategoriesUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});

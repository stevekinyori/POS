'use strict';

describe('Controller Tests', function() {

    describe('Suppliers Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockSuppliers, MockGoodsReturnedToSupplier, MockGoodsReceivedFromSupplier, MockSupplierProducts;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockSuppliers = jasmine.createSpy('MockSuppliers');
            MockGoodsReturnedToSupplier = jasmine.createSpy('MockGoodsReturnedToSupplier');
            MockGoodsReceivedFromSupplier = jasmine.createSpy('MockGoodsReceivedFromSupplier');
            MockSupplierProducts = jasmine.createSpy('MockSupplierProducts');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Suppliers': MockSuppliers,
                'GoodsReturnedToSupplier': MockGoodsReturnedToSupplier,
                'GoodsReceivedFromSupplier': MockGoodsReceivedFromSupplier,
                'SupplierProducts': MockSupplierProducts
            };
            createController = function() {
                $injector.get('$controller')("SuppliersDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'posApp:suppliersUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});

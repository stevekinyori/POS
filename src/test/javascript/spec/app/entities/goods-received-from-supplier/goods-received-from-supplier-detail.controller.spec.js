'use strict';

describe('Controller Tests', function() {

    describe('GoodsReceivedFromSupplier Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockGoodsReceivedFromSupplier, MockSuppliers;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockGoodsReceivedFromSupplier = jasmine.createSpy('MockGoodsReceivedFromSupplier');
            MockSuppliers = jasmine.createSpy('MockSuppliers');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'GoodsReceivedFromSupplier': MockGoodsReceivedFromSupplier,
                'Suppliers': MockSuppliers
            };
            createController = function() {
                $injector.get('$controller')("GoodsReceivedFromSupplierDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'posApp:goodsReceivedFromSupplierUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});

'use strict';

describe('Controller Tests', function() {

    describe('GoodsReturnedToSupplier Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockGoodsReturnedToSupplier, MockSuppliers;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockGoodsReturnedToSupplier = jasmine.createSpy('MockGoodsReturnedToSupplier');
            MockSuppliers = jasmine.createSpy('MockSuppliers');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'GoodsReturnedToSupplier': MockGoodsReturnedToSupplier,
                'Suppliers': MockSuppliers
            };
            createController = function() {
                $injector.get('$controller')("GoodsReturnedToSupplierDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'posApp:goodsReturnedToSupplierUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});

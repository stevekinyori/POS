'use strict';

describe('Controller Tests', function() {

    describe('SupplierProducts Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockSupplierProducts, MockProducts, MockSuppliers;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockSupplierProducts = jasmine.createSpy('MockSupplierProducts');
            MockProducts = jasmine.createSpy('MockProducts');
            MockSuppliers = jasmine.createSpy('MockSuppliers');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'SupplierProducts': MockSupplierProducts,
                'Products': MockProducts,
                'Suppliers': MockSuppliers
            };
            createController = function() {
                $injector.get('$controller')("SupplierProductsDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'posApp:supplierProductsUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});

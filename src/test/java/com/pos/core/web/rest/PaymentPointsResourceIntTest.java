package com.pos.core.web.rest;

import com.pos.core.PosApp;

import com.pos.core.domain.PaymentPoints;
import com.pos.core.repository.PaymentPointsRepository;
import com.pos.core.service.PaymentPointsService;
import com.pos.core.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pos.core.domain.enumeration.PAYMENT_TYPES;
import com.pos.core.domain.enumeration.Banks;
/**
 * Test class for the PaymentPointsResource REST controller.
 *
 * @see PaymentPointsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PosApp.class)
public class PaymentPointsResourceIntTest {

    private static final PAYMENT_TYPES DEFAULT_PREFERRED_PAYMENT_OPTION = PAYMENT_TYPES.BANK;
    private static final PAYMENT_TYPES UPDATED_PREFERRED_PAYMENT_OPTION = PAYMENT_TYPES.MPESA;

    private static final Banks DEFAULT_BANK_NAME = Banks.KCB;
    private static final Banks UPDATED_BANK_NAME = Banks.EQUITY;

    private static final String DEFAULT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NAME = "BBBBBBBBBB";

    @Autowired
    private PaymentPointsRepository paymentPointsRepository;

    @Autowired
    private PaymentPointsService paymentPointsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPaymentPointsMockMvc;

    private PaymentPoints paymentPoints;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PaymentPointsResource paymentPointsResource = new PaymentPointsResource(paymentPointsService);
        this.restPaymentPointsMockMvc = MockMvcBuilders.standaloneSetup(paymentPointsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentPoints createEntity(EntityManager em) {
        PaymentPoints paymentPoints = new PaymentPoints()
            .preferredPaymentOption(DEFAULT_PREFERRED_PAYMENT_OPTION)
            .bankName(DEFAULT_BANK_NAME)
            .accountNo(DEFAULT_ACCOUNT_NO)
            .accountName(DEFAULT_ACCOUNT_NAME);
        return paymentPoints;
    }

    @Before
    public void initTest() {
        paymentPoints = createEntity(em);
    }

    @Test
    @Transactional
    public void createPaymentPoints() throws Exception {
        int databaseSizeBeforeCreate = paymentPointsRepository.findAll().size();

        // Create the PaymentPoints
        restPaymentPointsMockMvc.perform(post("/api/payment-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentPoints)))
            .andExpect(status().isCreated());

        // Validate the PaymentPoints in the database
        List<PaymentPoints> paymentPointsList = paymentPointsRepository.findAll();
        assertThat(paymentPointsList).hasSize(databaseSizeBeforeCreate + 1);
        PaymentPoints testPaymentPoints = paymentPointsList.get(paymentPointsList.size() - 1);
        assertThat(testPaymentPoints.getPreferredPaymentOption()).isEqualTo(DEFAULT_PREFERRED_PAYMENT_OPTION);
        assertThat(testPaymentPoints.getBankName()).isEqualTo(DEFAULT_BANK_NAME);
        assertThat(testPaymentPoints.getAccountNo()).isEqualTo(DEFAULT_ACCOUNT_NO);
        assertThat(testPaymentPoints.getAccountName()).isEqualTo(DEFAULT_ACCOUNT_NAME);
    }

    @Test
    @Transactional
    public void createPaymentPointsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = paymentPointsRepository.findAll().size();

        // Create the PaymentPoints with an existing ID
        paymentPoints.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaymentPointsMockMvc.perform(post("/api/payment-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentPoints)))
            .andExpect(status().isBadRequest());

        // Validate the PaymentPoints in the database
        List<PaymentPoints> paymentPointsList = paymentPointsRepository.findAll();
        assertThat(paymentPointsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkPreferredPaymentOptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentPointsRepository.findAll().size();
        // set the field null
        paymentPoints.setPreferredPaymentOption(null);

        // Create the PaymentPoints, which fails.

        restPaymentPointsMockMvc.perform(post("/api/payment-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentPoints)))
            .andExpect(status().isBadRequest());

        List<PaymentPoints> paymentPointsList = paymentPointsRepository.findAll();
        assertThat(paymentPointsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBankNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentPointsRepository.findAll().size();
        // set the field null
        paymentPoints.setBankName(null);

        // Create the PaymentPoints, which fails.

        restPaymentPointsMockMvc.perform(post("/api/payment-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentPoints)))
            .andExpect(status().isBadRequest());

        List<PaymentPoints> paymentPointsList = paymentPointsRepository.findAll();
        assertThat(paymentPointsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAccountNoIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentPointsRepository.findAll().size();
        // set the field null
        paymentPoints.setAccountNo(null);

        // Create the PaymentPoints, which fails.

        restPaymentPointsMockMvc.perform(post("/api/payment-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentPoints)))
            .andExpect(status().isBadRequest());

        List<PaymentPoints> paymentPointsList = paymentPointsRepository.findAll();
        assertThat(paymentPointsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAccountNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentPointsRepository.findAll().size();
        // set the field null
        paymentPoints.setAccountName(null);

        // Create the PaymentPoints, which fails.

        restPaymentPointsMockMvc.perform(post("/api/payment-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentPoints)))
            .andExpect(status().isBadRequest());

        List<PaymentPoints> paymentPointsList = paymentPointsRepository.findAll();
        assertThat(paymentPointsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPaymentPoints() throws Exception {
        // Initialize the database
        paymentPointsRepository.saveAndFlush(paymentPoints);

        // Get all the paymentPointsList
        restPaymentPointsMockMvc.perform(get("/api/payment-points?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(paymentPoints.getId().intValue())))
            .andExpect(jsonPath("$.[*].preferredPaymentOption").value(hasItem(DEFAULT_PREFERRED_PAYMENT_OPTION.toString())))
            .andExpect(jsonPath("$.[*].bankName").value(hasItem(DEFAULT_BANK_NAME.toString())))
            .andExpect(jsonPath("$.[*].accountNo").value(hasItem(DEFAULT_ACCOUNT_NO.toString())))
            .andExpect(jsonPath("$.[*].accountName").value(hasItem(DEFAULT_ACCOUNT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getPaymentPoints() throws Exception {
        // Initialize the database
        paymentPointsRepository.saveAndFlush(paymentPoints);

        // Get the paymentPoints
        restPaymentPointsMockMvc.perform(get("/api/payment-points/{id}", paymentPoints.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(paymentPoints.getId().intValue()))
            .andExpect(jsonPath("$.preferredPaymentOption").value(DEFAULT_PREFERRED_PAYMENT_OPTION.toString()))
            .andExpect(jsonPath("$.bankName").value(DEFAULT_BANK_NAME.toString()))
            .andExpect(jsonPath("$.accountNo").value(DEFAULT_ACCOUNT_NO.toString()))
            .andExpect(jsonPath("$.accountName").value(DEFAULT_ACCOUNT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPaymentPoints() throws Exception {
        // Get the paymentPoints
        restPaymentPointsMockMvc.perform(get("/api/payment-points/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePaymentPoints() throws Exception {
        // Initialize the database
        paymentPointsService.save(paymentPoints);

        int databaseSizeBeforeUpdate = paymentPointsRepository.findAll().size();

        // Update the paymentPoints
        PaymentPoints updatedPaymentPoints = paymentPointsRepository.findOne(paymentPoints.getId());
        updatedPaymentPoints
            .preferredPaymentOption(UPDATED_PREFERRED_PAYMENT_OPTION)
            .bankName(UPDATED_BANK_NAME)
            .accountNo(UPDATED_ACCOUNT_NO)
            .accountName(UPDATED_ACCOUNT_NAME);

        restPaymentPointsMockMvc.perform(put("/api/payment-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPaymentPoints)))
            .andExpect(status().isOk());

        // Validate the PaymentPoints in the database
        List<PaymentPoints> paymentPointsList = paymentPointsRepository.findAll();
        assertThat(paymentPointsList).hasSize(databaseSizeBeforeUpdate);
        PaymentPoints testPaymentPoints = paymentPointsList.get(paymentPointsList.size() - 1);
        assertThat(testPaymentPoints.getPreferredPaymentOption()).isEqualTo(UPDATED_PREFERRED_PAYMENT_OPTION);
        assertThat(testPaymentPoints.getBankName()).isEqualTo(UPDATED_BANK_NAME);
        assertThat(testPaymentPoints.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testPaymentPoints.getAccountName()).isEqualTo(UPDATED_ACCOUNT_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingPaymentPoints() throws Exception {
        int databaseSizeBeforeUpdate = paymentPointsRepository.findAll().size();

        // Create the PaymentPoints

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPaymentPointsMockMvc.perform(put("/api/payment-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentPoints)))
            .andExpect(status().isCreated());

        // Validate the PaymentPoints in the database
        List<PaymentPoints> paymentPointsList = paymentPointsRepository.findAll();
        assertThat(paymentPointsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePaymentPoints() throws Exception {
        // Initialize the database
        paymentPointsService.save(paymentPoints);

        int databaseSizeBeforeDelete = paymentPointsRepository.findAll().size();

        // Get the paymentPoints
        restPaymentPointsMockMvc.perform(delete("/api/payment-points/{id}", paymentPoints.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PaymentPoints> paymentPointsList = paymentPointsRepository.findAll();
        assertThat(paymentPointsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PaymentPoints.class);
        PaymentPoints paymentPoints1 = new PaymentPoints();
        paymentPoints1.setId(1L);
        PaymentPoints paymentPoints2 = new PaymentPoints();
        paymentPoints2.setId(paymentPoints1.getId());
        assertThat(paymentPoints1).isEqualTo(paymentPoints2);
        paymentPoints2.setId(2L);
        assertThat(paymentPoints1).isNotEqualTo(paymentPoints2);
        paymentPoints1.setId(null);
        assertThat(paymentPoints1).isNotEqualTo(paymentPoints2);
    }
}
